'''

@author (C) 2013 Gerardo Ezquerra (catwashere@yahoo.com)
@file sample_feedback.py
@version 0.1

'''

import sys
sys.path.insert(0, 'library')

from feedback import APNS_Feedback

feedback = APNS_Feedback(APNS_Feedback.ENVIRONMENT_PRODUCTION, "certificate-prod.pem")

feedback.setRootCertificationAuthority("entrust_root_certification_authority.pem")

feedback.connect()

aDeviceTokens = feedback.receive()
if len(aDeviceTokens) > 0:
	print(aDeviceTokens)

feedback.disconnect()