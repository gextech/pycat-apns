'''

@author (C) 2013 Gerardo Ezquerra (catwashere@yahoo.com)
@file sample_server.py
@version 0.1

'''

import sys
sys.path.insert(0, 'library')

from time import sleep
from server import APNS_Push_Server, APNS_Message

server = APNS_Push_Server(APNS_Push_Server.ENVIRONMENT_PRODUCTION, "certificate-prod.pem")

server.setRootCertificationAuthority("entrust_root_certification_authority.pem")

server.setProcesses(2)

server.start()

i = 1

while server.run():
	errors = server.getErrors()
	if len(errors) > 0:
		print(errors)


	if i <= 10:
		if i == 5:
			message = APNS_Message('ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff')
		else:
			message = APNS_Message('b4b6b1999e71bb4283494bd138945c5a8a1cbaf75e6aa6add1655756bf56371c')

		message.setBadge(i)
		server.add(message)

	i += 1

	sleep(server.MAIN_LOOP_USLEEP / 1000000.0)